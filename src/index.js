import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

//restore console because SOMEBODY has overriden it instead of using proper logging
var i = document.createElement('iframe');
i.style.display = 'none';
document.body.appendChild(i);
window.console = i.contentWindow.console;

console.log('Restored console');

const body = document.getElementsByTagName("body")[0];
const app = document.createElement('div');
app.id = 'root';
if (body) { body.prepend(app) }; 

ReactDOM.render(<App />, document.getElementById('root'));
