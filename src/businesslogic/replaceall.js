module.exports = (value, search, replace)=>{
  return (value+"").split(search).join(replace);
}
