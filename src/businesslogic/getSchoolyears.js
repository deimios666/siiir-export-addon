const getSchoolyears = async ()=>{
  const listUrl = "https://www.siiir.edu.ro/siiir/list/schoolYear.json";
  const schoolYearResponse = await fetch(listUrl + "?_dc=" + (new Date()).getTime(),
            {
                method: 'post',
                credentials: 'same-origin',
                headers: {
                  "Accept": "application/json",
                  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                },
                body: "page=1&start=0&limit=100"
            });
  const schoolYearJSON = await schoolYearResponse.json();
  return Promise.resolve(schoolYearJSON.page.content);
}


export default getSchoolyears;
