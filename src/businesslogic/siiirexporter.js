import async from 'async';
import asyncify from 'async/asyncify'; //need to use asyncify because babel wrecks ES2017 async/await
import fetch from 'node-fetch';
import JSZip from 'jszip';
import saveAs from 'file-saver';

import replaceAll from './replaceall';

const headers = {
  "Accept": "application/json",
  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
}

const simplesiiirexporter = async (
  {
    maxParallel=4,
    schoolYearId=22,
    pagesize=300,
    listUrl = "",
    listParamString = "",
    csvHeaders = [],
    csvValues = [],
    progress
  }
  )=>{

  console.log('Getting max value');
  //get max values
  const primingResponse = await fetch(listUrl + "?_dc=" + (new Date()).getTime(),
            {
                method: 'post',
                credentials: 'same-origin',
                headers: headers,
                body: listParamString + "&page=1&start=0&limit=1"
            });
  const primingValues = await primingResponse.json();
  const maxValue = primingValues.page.total;
  let currentValue = 0;
  let updateProgressTimer;

  console.log('Checking for timer');
  if (typeof progress !== 'undefined'){
    console.log('Setting timer');
    updateProgressTimer = window.setInterval(()=>{
      console.log('Setting progress: ',Math.round(currentValue/maxValue*100));
      progress(Math.round(currentValue/maxValue*100));
    },1000);
  }

  console.log('Max value: ',maxValue);
  //prepare pages - 
  const maxPages = Math.ceil(maxValue / pagesize);
  let pages=[];
  for (let pageCounter = 0; pageCounter < maxPages; pageCounter++) {
    pages[pageCounter] = {
        page: pageCounter + 1,
        start: pageCounter * pagesize
    };
  }
  console.log('Prepped pages: ',pages);
  
  //fetch data async
  const csvData = await async.mapLimit(
    pages, //iterate over pages
    maxParallel, //in parallel
    asyncify(async item=>{
      const pageResponse = await fetch(listUrl + "?_dc=" + (new Date()).getTime(), {
                method: 'post',
                credentials: 'same-origin',
                headers: {
                    "Accept": "application/json",
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                },
                body: listParamString + "&page=" + item.page + "&start=" + item.start + "&limit=" + pagesize
            });
      const pageJSON = await pageResponse.json();

      console.log('Got page:',item)

      const csvPage = pageJSON.page.content.map(item => {
        const oneLine = csvValues.map( path => {
          try {
            //since js doesn't accept obj["subobj.subsubobj"] notation I split-reduce it to obj[subobj][subsubobj]
            let value = path.split(`.`).reduce((obj,key)=>obj[key],item);
            //console.log(value);
            if (typeof value == "string") {
                 value = replaceAll(value,`"`, `""`); //escaping quotes by doubling according to CSV spec
                 value = replaceAll(value,"\n", ` `); //newlines cause problems so we replace them too
            }
            if (value == null) {
                return `""`;
            } else {
                return `"${value}"`;
            }
          } catch (err){
            console.log('Error: ',err);
            return `""`;
          }
        })
        .join(`,`); //flatten the CSV line into a string
        //console.log(oneLine);
        currentValue=currentValue+1;
        return oneLine;
      });
      //console.log(csvPage);
      console.log('Done wtih page: ',item);
      return Promise.resolve(csvPage.join("\n")); //flatten the CSV lines into a string
    }));

  console.log(csvData);

  //prep CSV, prepend BOM since MS Excel has troubles with UTF8
  const csvString = "\uFEFF" + csvHeaders.join(",") + "\n" + csvData.join("\n");

  console.log('Got CSVString, returning it');

  if (typeof progress !== 'undefined'){
    console.log('Clearing timer');
    window.clearInterval(updateProgressTimer);
    console.log('Setting final progress: ',Math.round(currentValue/maxValue*100));
    progress(Math.round(currentValue/maxValue*100));
  }

  //return CSV
  return Promise.resolve(csvString);
}

const multisiiirexporter = async ({schoolYearId,exportRetea, exportFormatiuni, exportElevi, exportCladiri, exportCladiriAsoc, setExportReteaProgress, setExportFormatiuniProgress, setExportEleviProgress})=>{
  const params = {
    retea: {
      listUrl: "https://www.siiir.edu.ro/siiir/list/school.json",
      listParamString: "generatorKey=ASQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A" + schoolYearId + "%7D",
      csvHeaders: ["ID", "Tip Unitate", "Mediul", "CodFiscal", "CodSIIIR", "Nr HCL", "Data HCL", "Denumire", "Statut", "Cod PJ", "Cod SIRUTA" ,"Localitatea", "Localitatea Superioară", "Strada", "Nr", "Telefon", "Fax", "Email"],
      csvValues:["id", "schoolType.description", "locality.environment.description", "fiscalCode", "code", "hclNo", "hclDate", "longName", "statut.code", "parentSchool.code", "locality.sirutaCode", "locality.description", "locality.parentLocality.description", "street", "streetNumber", "phoneNumber", "faxNumber", "email"],
      pagesize: 100,
      schoolYearId: schoolYearId,
      progress: setExportReteaProgress
    },
    formatiuni: {
      listUrl: "https://www.siiir.edu.ro/siiir/list/studyFormation.json",
      listParamString: "generatorKey=ASSQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A"+schoolYearId+"%7D",
      csvHeaders: ["ID","ID plan","Descriere","Cod Unitate"],
      csvValues:["id","schoolPlanForStudyFormation.id","description", "school.code"],
      pagesize: 300,
      schoolYearId: schoolYearId,
      progress: setExportFormatiuniProgress
    },
    elevi : {
      listUrl: "https://www.siiir.edu.ro/siiir/list/asocStudentAtSchoolView.json",
      listParamString: "generatorKey=SFQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A"+schoolYearId+"%7D",
      csvHeaders: ["IDStudent","Cod PJ","Cod Unitate Asociat","CNP","Naționalitate","Limba","Cod Formațiune","Asociat la formațiune"],
      csvValues:["student.id","schoolCode","school.code","student.nin","student.nationality.description","student.language.description","formationCode","associated"],
      pagesize: 200,
      schoolYearId: schoolYearId,
      progress: setExportEleviProgress
    }
  }

  const exportArray = [];

  if (exportRetea){
    const reteaCSV = await simplesiiirexporter({...params.retea});
    exportArray.push({
      name: 'Retea.csv',
      content: reteaCSV
    });
    console.log('Got reteaCSV');
  }

  if (exportFormatiuni){
    const formatiuniCSV = await simplesiiirexporter({...params.formatiuni});
    exportArray.push({
      name: 'Formatiuni.csv',
      content: formatiuniCSV
    });
    console.log('Got formatiuniCSV');
  }

  
  
  //add to zip
  const zip = new JSZip();

  exportArray.forEach(
    item=>zip.file(item.name,item.content)
  );

  const zipcontent = await zip.generateAsync({type:"blob",compression: "DEFLATE"});

  console.log('Got zip contents, saving');

  saveAs(zipcontent,"export-siiir.zip");

  //download zip
}


export default multisiiirexporter;
