import React, { useState } from 'react';

import { 
   Button,
   Checkbox,
   Divider,
   FormControl,
   FormControlLabel,
   FormGroup,
   InputLabel,
   LinearProgress,
   MenuItem,
   Select,
   SwipeableDrawer
} from '@material-ui/core';

import './App.css';

import getSchoolyears from './businesslogic/getSchoolyears';
import siiirexporter from './businesslogic/siiirexporter';

const App = () => {

  const [state,setState] = useState({
    drawerOpened:false,
    exportRetea: false,
    exportReteaProgress: 0,
    exportFormatiuni: false,
    exportFormatiuniProgress: 0,
    exportElevi: false,
    exportEleviProgress: 0,
    exportCladiri: false,
    exportCladiriAsoc: false,
    schoolYearId: 22,
    schoolYears: [{id:22,code:"2019-2020"}]
  });

  const handleDrawerOpen = async () =>{
    
    if (state.schoolYears.length<2){
      //if initial opening get the schoolyear nomenclator  and calculate current year
      const schoolYears = await getSchoolyears();
      const schoolYearId = schoolYears.filter(schoolyear=>schoolyear.currentYear)[0];
      setState({ ...state, drawerOpened:true, schoolYearId:schoolYearId, schoolYears:schoolYears});
    } else {
      setState({ ...state, drawerOpened:true});
    }
    
  }

  const handleDrawerClose = () =>{
    setState({ ...state, drawerOpened:false});
  }

  const handleCheck = name => event =>{
    setState({ ...state, [name]: event.target.checked });
  }

  const handleSchoolYearChange = event =>{
    setState({...state, schoolYearId:event.target.value});
  }

  const setExportReteaProgress = progress => {
    setState({ ...state, exportReteaProgress:progress});
  }

  const setExportFormatiuniProgress = progress => {
    setState({ ...state, exportFormatiuniProgress:progress});
  }

  const setExportEleviProgress = progress => {
    setState({ ...state, exportEleviProgress:progress});
  }

  const doExport = ()=>{
    const {schoolYearId, exportRetea, exportFormatiuni, exportElevi, exportCladiri, exportCladiriAsoc} = state;
    return siiirexporter({schoolYearId, exportRetea, exportFormatiuni, exportElevi, exportCladiri, exportCladiriAsoc, setExportReteaProgress, setExportFormatiuniProgress, setExportEleviProgress});
  };


  return (
    <div className="App">
      <Button 
        variant="contained" 
        color="primary"
        onClick={handleDrawerOpen}
      >
      Exp
      </Button>
      
      <SwipeableDrawer
        open={state.drawerOpened}
        onOpen={handleDrawerOpen}
        onClose={handleDrawerClose}>
        <FormGroup>
          <FormControl>
            <InputLabel id="schoolyear-select-label">An Școlar</InputLabel>
            <Select
              labelId="schoolyear-select-label"
              id="schoolyear-select"
              value={state.schoolYearId}
              onChange={handleSchoolYearChange}
            >
              {state.schoolYears.map(schoolYear=>
                <MenuItem key={schoolYear.id} value={schoolYear.id}>{schoolYear.code}</MenuItem>)
              }
            </Select>
          </FormControl>

          <FormControlLabel control={<Checkbox value="exportRetea" checked={state.exportRetea} onChange={handleCheck('exportRetea')} />} label="Rețea școlară" />
          <LinearProgress variant="determinate" value={state.exportReteaProgress} />
          <FormControlLabel control={<Checkbox value="exportFormatiuni" checked={state.exportFormatiuni} onChange={handleCheck('exportFormatiuni')} />} label="Formațiuni de studiu" />
          <LinearProgress variant="determinate" value={state.exportFormatiuniProgress} />
          <FormControlLabel control={<Checkbox value="exportElevi" checked={state.exportElevi} onChange={handleCheck('exportElevi')}/>} label="Elevi" />
          <LinearProgress variant="determinate" value={state.exportEleviProgress} />
          <FormControlLabel control={<Checkbox value="exportCladiri" checked={state.exportCladiri} onChange={handleCheck('exportCladiri')}/>} label="Clădiri" />
          <FormControlLabel control={<Checkbox value="exportCladiriAsoc" checked={state.exportCladiriAsoc} onChange={handleCheck('exportCladiriAsoc')}/>} label="Asociere Clădiri" />

          <Divider />

          <FormControlLabel control={<Checkbox value="checkedC" />} label="Anonimizat" />

          <Button 
            variant="contained" 
            color="primary"
            onClick={doExport}
          >
          Exportă
          </Button>

        </FormGroup>
      </SwipeableDrawer>
    </div>
  );
}

export default App;
