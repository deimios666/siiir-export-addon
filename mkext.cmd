rem npm run build
rmdir /s /q siiirexport.extension\static
xcopy /e build\static siiirexport.extension\static\
pushd siiirexport.extension\static\css
ren *.css main.css
popd
pushd siiirexport.extension\static\js
ren main.*.js main.js
ren runtime-main.*.js runtime-main.js
ren *.chunk.js chunk.js
popd
