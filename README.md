# SIIIR export addon

A chrome addon that exports data from SIIIR (Romanian Education System)

# Getting started

```
npm i
mkext
```

Open chrome, Load unpacked extension from the siiirexport.extension directory.

Navigate to https://siiir.edu.ro/siiir/ and notice the "Exp" button in the top left.
